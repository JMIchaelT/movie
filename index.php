<?php
    require_once 'php/init.php';
    error_reporting (E_ALL ^ E_NOTICE); 
   
?>

<!doctype html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="vendor/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
   <link href="vendor/css/all.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
     <link rel="stylesheet" type="text/css" href="resource/css/style.css">
    <title>Odeoncinema.com</title>
  </head>
  <body onload="test()">
 <section class="section1">
  <div class="mySidenav" id="sidenav">
      <a href="landing.php" class="home" id="Home"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" color="#4D3300" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>&nbsp;&nbsp;Home</a>
    <div class=" shadow">
        <span class="navbar-brand mb-0 h1">Cinema 1</span>
    </div>

    <ul class="showcase">
      <li>
        <div class="seat"></div>
        <small>Available</small>
      </li>
      <li>
        <div class="seat selected"></div>
        <small>Selected</small>
      </li>
      <li>
        <div class="seat occupied"></div>
        <small>Occupied</small>
      </li>
    </ul>

    <div class="container">
      <div class="screen"><h3>Screen</h3></div>

      <div class="row">
        <div class="seat <?php hey();?>" id="items" value="1A">1A</div>
        <div class="seat <?php hey1();?>"  value="2A">2A</div>
        <div class="seat <?php hey2();?>"  value="3A">3A</div>
        <div class="seat <?php hey3();?>" value="4A">4A</div>
        <div class="seat <?php hey4();?>" value="5A">5A</div>
        <div class="seat <?php hey5();?>" value="6A">6A</div>
        <div class="seat <?php hey6();?>" value="7A">7A</div>
        <div class="seat <?php hey7();?>" value="8A">8A</div>
      </div>

      <div class="row">
        <div class="seat <?php heyB();?>" value="1B">1B</div>
        <div class="seat <?php heyB1();?>" value="2B">2B</div>
        <div class="seat <?php heyB2();?>" value="3B">3B</div>
        <div class="seat <?php heyB3();?>" value="4B">4B</div>
        <div class="seat <?php heyB4();?>" value="5B">5B</div>
        <div class="seat <?php heyB5();?>" value="6B">6B</div>
        <div class="seat <?php heyB6();?>" value="7B">7B</div>
        <div class="seat <?php heyB7();?>" value="8B">8B</div>
      </div>
      <div class="row">
        <div class="seat <?php heyC();?>" value="1C">1C</div>
        <div class="seat <?php heyC1();?>" value="2C">2C</div>
        <div class="seat <?php heyC2();?>" value="3C">3C</div>
        <div class="seat <?php heyC3();?>" value="4C">4C</div>
        <div class="seat <?php heyC4();?>" value="5C">5C</div>
        <div class="seat <?php heyC5();?>" value="6C">6C</div>
        <div class="seat <?php heyC6();?>" value="7C">7C</div>
        <div class="seat <?php heyC7();?>" value="8C">8C</div>
      </div>

      <div class="row">
        <div class="seat <?php heyD();?>" value="1D">1D</div>
        <div class="seat <?php heyD1();?>" value="2D">2D</div>
        <div class="seat <?php heyD2();?>" value="3D">3D</div>
        <div class="seat <?php heyD3();?>" value="4D">4D</div>
        <div class="seat <?php heyD4();?>" value="5D">5D</div>
        <div class="seat <?php heyD5();?>" value="6D">6D</div>
        <div class="seat <?php heyD6();?>" value="7D">7D</div>
        <div class="seat <?php heyD7();?>" value="8D">8D</div>
      </div>

      <div class="row">
        <div class="seat <?php heyE();?>" value="1E">1E</div>
        <div class="seat <?php heyE1();?>" value="2E">2E</div>
        <div class="seat <?php heyE2();?>" value="3E">3E</div>
        <div class="seat <?php heyE3();?>" value="4E">4E</div>
        <div class="seat <?php heyE4();?>" value="5E">5E</div>
        <div class="seat <?php heyE5();?>" value="6E">6E</div>
        <div class="seat <?php heyE6();?>" value="7E">7E</div>
        <div class="seat <?php heyE7();?>" value="8E">8E</div>
      </div>

      <div class="row">
        <div class="seat <?php heyF();?>" value="1F">1F</div>
        <div class="seat <?php heyF1();?>" value="2F">2F</div>
        <div class="seat <?php heyF2();?>" value="3F">3F</div>
        <div class="seat <?php heyF3();?>" value="4F">4F</div>
        <div class="seat <?php heyF4();?>" value="5F">5F</div>
        <div class="seat <?php heyF5();?>" value="6F">6F</div>
        <div class="seat <?php heyF6();?>" value="7F">7F</div>
        <div class="seat <?php heyF7();?>" value="8F">8F</div>
      </div>

    <p class="text">
      You have selected <span id="count">0</span> seat for a price of ₱ <span id="total">0</span>
    </p>
 

   
      <div class="container">
      <form class="form" acion="" method="GET">
        <div class="row high">
          <div class="col-md-9 form-group">
            <label>Name:</label>
              <input class="form-control low" type="text" name="name" placeholder="name" required />
          </div>
          <div class="col-md-9 form-group">
            <label>Seat:</label>
              <input class="form-control low" type="text" id="s1" name="seat" placeholder="row" value="" required />
          </div>

          <div class="col-md-9 form-group">
            <label>Price:</label>
              <input class="form-control low" type="text" name="price" id="price" placeholder="price" value="" required />
          </div>

          <div class="col-md-9 form-group">
            <label>Movie:</label>
            <select id="movie" name="movie">
              <option value="Patikim ng Pinya">Patikim ng Pinya</option>
              <option value="Adan">Adan</option>
              <option value="Hibla">Hibla</option>
              <option value="Silip">Silip</option>
            </select>
          </div>

          <div class="col-md-9 form-group">
            <label>Time:</label>
            <select  id="time" name="time">
              <option value="8:30pm">8:00pm</option>
              <option value="9:30pm">9:30pm</option>
              <option value="10:30pm">10:30pm</option>
              <option value="11:30pm">11:30pm</option>
            </select>
          </div>

          <div class="col-md-9 form-group">
          <label>Date:</label>
            <input type="date" id="date" name="date">
          </div>

            <div class="col-md-4">
              <input class ="btn sabmit" type="submit" value="Save" />
            </div>
            <div class="confirmpage">
              <a href="confirmation.php">confirm booking</a>
            </div>
        </div>
      </form>
  </div>
  <?php   waiter();?>
</section>
<footer>
    <div class="container">
      <div class="row">
        <div class="col-md">
          <p class="ft2">
            CTTO for pictures, For educational Purposes Only<br/>&nbsp;&nbsp;&nbsp;
            all rights reserve 2021<br/>&nbsp;&nbsp;&nbsp;
            <i class="fab fa-facebook-square"></i>
            <i class="fab fa-twitter-square"></i>
            <i class="fab fa-instagram"></i>
            <i class="fab fa-youtube"></i>
          </p>
        </div>
      </div>
    </div>
  </footer>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
     <script src="resource/js/script.js"></script>
     <script src="vendor/js/jquery.js"></script>
  <script src="vendor/js/popper.js"></script>
  <script src="vendor/js/bootstrap.min.js"></script>
  
  </body>
</html>